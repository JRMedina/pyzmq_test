# cat_group.capnp
@0xec62e2330924533c;

enum Breed {
    siamese @0;
    maineCoon @1;
    sacredBirman @2;
}

struct Cat {
    name @0 :Text;
    breed @1 :Breed;
}

struct CatGroup {
    cats @0 :List(Cat);
}