docker :
	docker build -t pyzmq_test .

proto :
	protoc --proto_path=protobuf --python_out=pyzmq_test protobuf/cat_group.proto

proto_bench :
	poetry run python -u pyzmq_test/communication_benchmarks/zmq_no_copy.py

orch :
	docker-compose -f docker-compose-orch.yml up


zmq-bench :
	docker-compose -f docker-compose.yml up