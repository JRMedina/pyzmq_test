FROM python:3.9

RUN apt-get update && \
    apt install -y protobuf-compiler gcc cmake capnproto

RUN pip install poetry

RUN mkdir /pyzmq_test; \
    mkdir /pyzmq_test/pyzmq_test

COPY pyproject.toml /pyzmq_test/
COPY poetry.lock /pyzmq_test/
COPY pyzmq_test/__init__.py /pyzmq_test/pyzmq_test/

WORKDIR /pyzmq_test

RUN poetry install

COPY . .