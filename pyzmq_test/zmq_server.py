import os
import time
import json

import capnp
import zmq


context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:5555")

capnp.remove_import_hook()
cat_group_capnp = capnp.load("capnproto/cat_group.capnp")


while True:
    #  Wait for next request from client
    # start = time.time()
    # message = socket.recv()
    message = socket.recv_multipart()
    # print(f"Received request: {message}")
    # dict_structure = json.loads(message)
    # c_object = cattr.structure(dict_structure, C)
    # print(c_object)
    cats = cat_group_capnp.CatGroup.from_segments(message)
    # print(cats)
    # print(type(cats))
    # first_cat = cats.cats[0]
    # print(first_cat.name)
    # builder.cats[0].name = "bob"
    # print(builder.is_root)
    builder = cats.as_builder()
    # message_back = builder.to_bytes()
    message_back = builder.to_segments()

    #  Send reply back to client
    # socket.send(message_back)
    socket.send_multipart(message_back)
    # socket.send(message)
