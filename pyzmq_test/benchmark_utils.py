import sys

from typing import List

import numpy


def get_string_size_in_mb(str_to_check: str) -> float:
    return sys.getsizeof(str_to_check) * 0.000001


def print_data(data: List[float], data_name: str) -> None:
    print("=======")
    print(f"Stats for {data_name}")
    print("------")
    print(f"Average: {numpy.average(data) *  0.000001} ms")
    for percentile in [90, 95, 99, 99.9]:
        print(f"p{percentile}: {numpy.percentile(data, percentile) *  0.000001} ms")
