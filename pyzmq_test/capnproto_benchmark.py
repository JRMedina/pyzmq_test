import time

from typing import Callable

import capnp

from tqdm import tqdm

from pyzmq_test.benchmark_utils import get_string_size_in_mb, print_data
from pyzmq_test.capnproto_func import make_cats_request_size, make_cats_response_size


capnp.remove_import_hook()
cat_group_capnp = capnp.load("capnproto/cat_group.capnp")


CapnProtoObjectType = cat_group_capnp.CatGroup
MakeObjectType = Callable[[], cat_group_capnp.CatGroup]


def run(
    num_iterations: int,
    make_object_func: MakeObjectType,
    proto_object_class: CapnProtoObjectType,
) -> None:

    total_times = []
    unstruct_times = []
    struct_times = []

    capn_proto_obj = make_object_func()
    serialized_capn_proto = capn_proto_obj.to_bytes()
    print(
        f"Serialized capn proto is {get_string_size_in_mb(serialized_capn_proto)} MBs"
    )

    for _ in tqdm(range(num_iterations)):

        start = time.process_time_ns()
        string_capn_proto = capn_proto_obj.to_bytes()
        from_string = time.process_time_ns()
        proto_object_class.from_bytes(string_capn_proto)
        end = time.process_time_ns()

        total_times.append(end - start)
        unstruct_times.append(from_string - start)
        struct_times.append(end - from_string)

    print_data(total_times, "Proto Total Times")
    print_data(unstruct_times, "Proto Serial Times")
    print_data(struct_times, "Proto Deserial Times")


if __name__ == "__main__":
    run(1000, make_cats_request_size, cat_group_capnp.CatGroup)
    print("=================================")
    run(1000, make_cats_response_size, cat_group_capnp.CatGroup)
