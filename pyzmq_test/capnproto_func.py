import random
import string
import sys

import capnp

capnp.remove_import_hook()
cat_group_capnp = capnp.load("capnproto/cat_group.capnp")


def fill_cat(cat: cat_group_capnp.Cat) -> None:
    cat.breed = random.choice(["siamese", "maineCoon", "sacredBirman"])
    cat.name = "".join(random.choices(string.ascii_uppercase + string.digits, k=20))


def make_cats(num_cats: int) -> cat_group_capnp.CatGroup:

    cats = cat_group_capnp.CatGroup.new_message()
    cats.init("cats", num_cats)

    for cat in cats.cats:
        fill_cat(cat)
    return cats


def make_cats_response_size() -> cat_group_capnp.CatGroup:
    # 0.15 MB for 12 ads with vast
    return make_cats(3125)


def make_cats_request_size() -> cat_group_capnp.CatGroup:
    # 0.0015 mb
    return make_cats(60)


# cats = make_cats_response_size()
# print(sys.getsizeof(cats.to_bytes()))
# print(sys.getsizeof(cats.to_segments()))
