import random
import string

from enum import unique, Enum
from typing import Sequence

from attr import define


@unique
class CatBreed(Enum):
    SIAMESE = "siamese"
    MAINE_COON = "maine_coon"
    SACRED_BIRMAN = "birman"


@define
class Cat:
    breed: CatBreed
    names: Sequence[str]


def random_cat() -> Cat:
    return Cat(
        breed=random.choice(
            [CatBreed.SIAMESE, CatBreed.MAINE_COON, CatBreed.SACRED_BIRMAN]
        ),
        names=["".join(random.choices(string.ascii_uppercase + string.digits, k=20))],
    )
