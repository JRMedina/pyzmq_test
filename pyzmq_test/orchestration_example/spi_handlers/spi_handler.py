from typing import Union

import zmq

from pyzmq_test.orchestration_example.message_client import MessageClient, ObjectType


class SPIHandler:
    def __init__(
        self,
        name: str,
        req_or_rep: int,
        socket_context: zmq.Context,
        bind_address: str,
    ):
        self.name = name
        self.client = MessageClient(req_or_rep, socket_context, bind_address)

    def handle_response(self, obj: ObjectType) -> ObjectType:

        return obj

    def apply(self, obj: ObjectType) -> ObjectType:

        print(f"SPI Handler {self.name} sending work...")
        self.client.send(obj)
        obj_back = self.client.receive()
        obj_back = self.handle_response(obj)
        return obj_back
