import time

import capnp
import zmq


capnp.remove_import_hook()
cat_group_capnp = capnp.load("capnproto/cat_group.capnp")


def connect_server() -> zmq.Socket:
    print("Connecting to orchestrator...")
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://*:5555")
    return socket


def run_server():
    socket = connect_server()

    while True:
        #  Wait for next request from client
        print("Waiting for work from orchestrator...")
        message = socket.recv()

        print("Received work from orchestrator...")
        cats = cat_group_capnp.CatGroup.from_bytes(message)
        builder = cats.as_builder()
        time.sleep(5)

        #  Send reply back to client
        socket.send(builder.to_bytes())
        print("Sent result back to orchstrator...")


run_server()
