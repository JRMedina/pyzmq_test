CLEAN_ADDRESS = "ipc:///comms/clean.pipe"

CUT_ADDRESS = "ipc:///comms/cut.pipe"

EAT_ADDRESS = "ipc:///comms/eat.pipe"

TRASH_ADDRESS = "ipc:///comms/trash.pipe"

CLEAN_SPI = {"name": "CLEAN SPI Handler", "bind_address": CLEAN_ADDRESS}

CUT_SPI = {"name": "CUT SPI Handler", "bind_address": CUT_ADDRESS}

EAT_SPI = {"name": "EAT SPI Handler", "bind_address": EAT_ADDRESS}

TRASH_SPI = {"name": "TRASH SPI Handler", "bind_address": TRASH_ADDRESS}


CLEAN_SPI_IMP = {"name": "CLEAN SPI Imp", "bind_address": CLEAN_ADDRESS}

CUT_SPI_IMP = {"name": "CUT SPI Imp", "bind_address": CUT_ADDRESS}

EAT_SPI_IMP = {"name": "EAT SPI Imp", "bind_address": EAT_ADDRESS}

TRASH_SPI_IMP = {"name": "TRASH SPI Imp", "bind_address": TRASH_ADDRESS}
