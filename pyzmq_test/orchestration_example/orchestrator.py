import os

from typing import Dict

import capnp
import zmq

from pyzmq_test.capnproto_func import make_cats_request_size
from pyzmq_test.orchestration_example.workflows.workflow import Workflow
from pyzmq_test.orchestration_example.message_client import ObjectType

# capnp.remove_import_hook()
# cat_group_capnp = capnp.load("capnproto/cat_group.capnp")


# def connect_client() -> zmq.Socket:

#     #  Socket to talk to server
#     print("Connecting to SPI...")
#     context = zmq.Context()
#     socket = context.socket(zmq.REQ)
#     print(f"Connecting to spi @ {os.environ['SERVER_CONNECT_URI']}")
#     socket.connect(os.environ["SERVER_CONNECT_URI"])
#     return socket


# def run_client() -> None:

#     socket = connect_client()

#     capn_proto_obj = make_cats_request_size()
#     message = capn_proto_obj.to_bytes()

#     while True:

#         print("Sending work to SPI...")
#         socket.send(message)

#         #  Get the reply
#         print("Waiting for response from SPI...")
#         message = socket.recv()

#         print("Received SPI response")
#         cat_group_capnp.CatGroup.from_bytes(message)


# run_client()


class Orchestrator:
    def __init__(self, workflow_map: Dict[str, Workflow]) -> None:
        self.workflow_map = workflow_map

    def decide_workflow_strategy(self, _: ObjectType) -> Workflow:
        return self.workflow_map["fruit"]

    def orchestrate(self, obj: ObjectType) -> ObjectType:

        print("Orchestrating work")
        workflow = self.decide_workflow_strategy(obj)
        result = workflow.execute(obj)
        return result
