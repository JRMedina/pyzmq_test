import random
from pyzmq_test.orchestration_example.config import (
    CLEAN_SPI,
    CUT_SPI,
    EAT_SPI,
    TRASH_SPI,
)

import zmq

from pyzmq_test.orchestration_example.message_client import ObjectType
from pyzmq_test.orchestration_example.workflows.workflow import Workflow
from pyzmq_test.orchestration_example.spi_handlers.spi_handler import SPIHandler


class FruitWorkflow(Workflow):
    def __init__(self, socket_context: zmq.Context) -> None:
        super().__init__()
        self.clean_spi = SPIHandler(
            CLEAN_SPI["name"], zmq.REQ, socket_context, CLEAN_SPI["bind_address"]
        )
        self.cut_spi = SPIHandler(
            CUT_SPI["name"], zmq.REQ, socket_context, CUT_SPI["bind_address"]
        )
        self.eat_spi = SPIHandler(
            EAT_SPI["name"], zmq.REQ, socket_context, EAT_SPI["bind_address"]
        )
        self.trash_spi = SPIHandler(
            TRASH_SPI["name"], zmq.REQ, socket_context, TRASH_SPI["bind_address"]
        )

    def execute(self, obj: ObjectType) -> ObjectType:

        # Always clean then cut
        obj = self.clean_spi.apply(obj)
        obj = self.cut_spi.apply(obj)

        # Check if food is dirty, throwing in trash if it is
        is_dirty = bool(random.getrandbits(1))
        if is_dirty:
            print("Fruit is dirty... Throwing in the trash")
            obj = self.trash_spi.apply(obj)

        # If clean, then eat and trash
        else:
            print("Fruit is clean, eating before trash...")
            obj = self.eat_spi.apply(obj)
            obj = self.trash_spi.apply(obj)

        return obj
