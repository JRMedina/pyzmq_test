from pyzmq_test.orchestration_example.message_client import ObjectType


class Workflow:
    def __init__(self) -> None:
        pass

    def execute(self, obj: ObjectType) -> ObjectType:
        pass
