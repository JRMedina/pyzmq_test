from typing import Union

import capnp
import zmq

capnp.remove_import_hook()
cat_group_capnp = capnp.load("capnproto/cat_group.capnp")

ObjectType = cat_group_capnp.CatGroup


class MessageClient:
    def __init__(
        self,
        req_or_rep: int,
        socket_context: zmq.Context,
        bind_address: str,
    ) -> None:

        self.socket = socket_context.socket(req_or_rep)
        if req_or_rep == zmq.REQ:
            self.socket.connect(bind_address)
        else:
            self.socket.bind(bind_address)

    def send(self, obj_to_send: ObjectType) -> None:

        bytes_obj = obj_to_send.to_bytes()
        self.socket.send(bytes_obj)

    def receive(self) -> ObjectType:

        bytes_obj = self.socket.recv()
        obj_back = ObjectType.from_bytes(bytes_obj)
        obj_back = obj_back.as_builder()
        return obj_back
