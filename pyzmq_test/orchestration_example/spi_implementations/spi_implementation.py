import time

import zmq

from pyzmq_test.orchestration_example.message_client import MessageClient, ObjectType


class SPIImplementation:
    def __init__(
        self,
        name: str,
        req_or_rep: int,
        socket_context: zmq.Context,
        bind_address: str,
    ):

        self.name = name
        self.client = MessageClient(req_or_rep, socket_context, bind_address)

    def handle_request(self) -> None:

        print(f"SPI {self.name} ready for work...")
        obj = self.client.receive()
        print(f"SPI {self.name} working...")
        time.sleep(1)
        self.client.send(obj)
