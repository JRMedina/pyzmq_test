import os

from typing import Dict

import zmq

from pyzmq_test.capnproto_func import make_cats_request_size
from pyzmq_test.orchestration_example.config import (
    CLEAN_SPI_IMP,
    CUT_SPI_IMP,
    EAT_SPI_IMP,
    TRASH_SPI_IMP,
)
from pyzmq_test.orchestration_example.orchestrator import Orchestrator
from pyzmq_test.orchestration_example.spi_implementations.spi_implementation import (
    SPIImplementation,
)
from pyzmq_test.orchestration_example.workflows.workflow import Workflow
from pyzmq_test.orchestration_example.workflows.workflow_fruit import FruitWorkflow


def initialize_workflows(context: zmq.Context) -> Dict[str, Workflow]:
    return {"fruit": FruitWorkflow(context)}


def initialize_orchestrator(workflow_map: Dict[str, Workflow]) -> Orchestrator:
    return Orchestrator(workflow_map)


def create_orch_work(orchestrator: Orchestrator) -> None:

    while True:
        cats = make_cats_request_size()
        orchestrator.orchestrate(cats)


def handle_orch_work(imp: SPIImplementation) -> None:

    while True:
        imp.handle_request()


if __name__ == "__main__":
    context = zmq.Context()

    if "ORCHESTRATION" in os.environ:
        print("Initializing orchestration")
        workflows = initialize_workflows(context)
        orchestrator = initialize_orchestrator(workflows)
        create_orch_work(orchestrator)

    imp = None
    if "CLEAN" in os.environ:
        print("Creating CLEAN SPI")
        imp = SPIImplementation(
            CLEAN_SPI_IMP["name"], zmq.REP, context, CLEAN_SPI_IMP["bind_address"]
        )
    elif "CUT" in os.environ:
        print("Creating CUT SPI")
        imp = SPIImplementation(
            CUT_SPI_IMP["name"], zmq.REP, context, CUT_SPI_IMP["bind_address"]
        )
    elif "EAT" in os.environ:
        print("Creating EAT SPI")
        imp = SPIImplementation(
            EAT_SPI_IMP["name"], zmq.REP, context, EAT_SPI_IMP["bind_address"]
        )
    elif "TRASH" in os.environ:
        print("Creating TRASH SPI")
        imp = SPIImplementation(
            TRASH_SPI_IMP["name"], zmq.REP, context, TRASH_SPI_IMP["bind_address"]
        )

    if imp:
        handle_orch_work(imp)
    else:
        print("Not orchestration or SPI!!")
        raise RuntimeError("Panic")
