import os
import time
from turtle import st

from typing import Callable

import capnp
import zmq

from google.protobuf.internal import api_implementation
from tqdm import tqdm

from pyzmq_test.benchmark_utils import get_string_size_in_mb, print_data
from pyzmq_test.cat_group_pb2 import CatGroup
from pyzmq_test.objects import Cat
from pyzmq_test.capnproto_func import make_cats_request_size, make_cats_response_size


capnp.remove_import_hook()
cat_group_capnp = capnp.load("capnproto/cat_group.capnp")


CapnProtoObjectType = cat_group_capnp.CatGroup
MakeObjectType = Callable[[], cat_group_capnp.CatGroup]


def connect() -> zmq.Socket:
    context = zmq.Context()

    #  Socket to talk to server
    print("Connecting to hello world server...")
    socket = context.socket(zmq.REQ)
    print(f"Connecting to server @ {os.environ['SERVER_CONNECT_URI']}")
    socket.connect(os.environ["SERVER_CONNECT_URI"])
    return socket


def run(
    num_iterations: int,
    make_object_func: MakeObjectType,
    proto_object_class: CapnProtoObjectType,
    socket: zmq.Socket,
) -> None:

    total_times = []
    send_times = []
    receive_times = []

    capn_proto_obj = make_object_func()
    serialized_capn_proto = capn_proto_obj.to_bytes()
    print(
        f"Serialized capn proto is {get_string_size_in_mb(serialized_capn_proto)} MBs"
    )

    for _ in tqdm(range(num_iterations)):

        # Send proto
        start = time.process_time_ns()
        # print(capn_proto_obj)
        # print(type(capn_proto_obj))
        string_capn_proto = capn_proto_obj.to_segments()
        # print(type(string_capn_proto))
        # socket.send(string_capn_proto)
        socket.send_multipart(string_capn_proto)
        time_send = time.process_time_ns()

        #  Get the reply
        # message = socket.recv()
        message = socket.recv_multipart()
        obj_back = proto_object_class.from_segments(message)
        # print(type(obj_back))
        obj_back.as_builder()
        # print(obj_back.cats[0])
        end = time.process_time_ns()

        total_times.append(end - start)
        send_times.append(time_send - start)
        receive_times.append(end - time_send)

    print_data(total_times, "Proto Total Times")
    print_data(send_times, "Proto Send Times")
    print_data(receive_times, "Proto Receive Times")


socket = connect()
run(10000, make_cats_request_size, cat_group_capnp.CatGroup, socket)
run(10000, make_cats_response_size, cat_group_capnp.CatGroup, socket)

# for cat_max in range(1000, 40000, 1000):

#     run_times = []
#     serial_times = []
#     unstruct_times = []
#     dump_times = []
#     load_times = []
#     struct_times = []

#     print("=======")
#     print(f"Num cats {cat_max}")
#     cats = make_cats(cat_max)
#     string_cats = cats.SerializeToString()
#     print(f"Cats is {cats.ByteSize() * 0.000001} MBs.")
#     print(f"{len(cats.cats)} cats is {get_string_size_in_mb(string_cats)} MBs")

#     for req_num in range(100):
#         start = time.time()
#         string_cats = cats.SerializeToString()
#         from_string = time.time()
#         cats_from_string = CatGroup()
#         cats_from_string.ParseFromString(string_cats)
#         end = time.time()

#         serial_times.append(end - start)
#         unstruct_times.append(from_string - start)
#         struct_times.append(end - from_string)
#         #  Do requests, waiting each time for a response
#         # print(f"Sending request {req_num}...")
#     #     start = time.time()
#     #     socket.send_string(string)

#     #     #  Get the reply.
#     #     message = socket.recv()
#     #     end = time.time()
#     #     run_times.append(end - start)
#     #     print(f"Received reply in {end-start} seconds")

#     # print(
#     #     f"Average transmission {sum(run_times)/len(run_times) * 1000} ms for {len(run_times)} runs"
#     # )

#     print(
#         f"Average struct {sum(struct_times)/len(serial_times) * 1000} ms for {len(serial_times)} runs"
#     )
#     print(
#         f"Average unstruct {sum(unstruct_times)/len(serial_times) * 1000} ms for {len(serial_times)} runs"
#     )
#     print(
#         f"Average serial {sum(serial_times)/len(serial_times) * 1000} ms for {len(serial_times)} runs"
#     )
