import attr


@attr.s(slots=True, frozen=True)  # It works with normal classes too.
class C:
    a = attr.ib()
    b = attr.ib()
