import os
import time

from typing import Callable

import capnp
import zmq

from pyzmq_test.benchmark_utils import get_string_size_in_mb, print_data
from pyzmq_test.capnproto_func import make_cats_request_size, make_cats_response_size


capnp.remove_import_hook()
cat_group_capnp = capnp.load("capnproto/cat_group.capnp")


CapnProtoObjectType = cat_group_capnp.CatGroup
MakeObjectType = Callable[[], cat_group_capnp.CatGroup]


def connect_server() -> zmq.Socket:
    print("Connecting to client...")
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("ipc:///comms/comm.pipe")
    return socket


def run_server():
    socket = connect_server()

    while True:
        #  Wait for next request from client
        message = socket.recv()
        # message = socket.recv_multipart(copy=False)
        # message = socket.recv(copy=False)
        cats = cat_group_capnp.CatGroup.from_bytes(message)
        # cats = cat_group_capnp.CatGroup.from_segments(message)
        builder = cats.as_builder()

        #  Send reply back to client
        socket.send(builder.to_bytes())
        # socket.send(builder.to_bytes(), copy=False)
        # socket.send_multipart(builder.to_segments(), copy=False)


def connect_client() -> zmq.Socket:
    context = zmq.Context()

    #  Socket to talk to server
    print("Connecting to server...")
    socket = context.socket(zmq.REQ)
    print(f"Connecting to server @ ipc:///comms/comm.pipe")
    socket.connect("ipc:///comms/comm.pipe")
    return socket


def run_client(
    num_iterations: int,
    make_object_func: MakeObjectType,
    proto_object_class: CapnProtoObjectType,
) -> None:

    socket = connect_client()

    total_times = []
    send_times = []
    receive_times = []

    capn_proto_obj = make_object_func()
    serialized_capn_proto = capn_proto_obj.to_bytes()
    print(
        f"Serialized capn proto is {get_string_size_in_mb(serialized_capn_proto)} MBs"
    )

    for _ in range(num_iterations):

        # Send proto
        start = time.process_time_ns()

        string_capn_proto = capn_proto_obj.to_bytes()
        # string_capn_proto = capn_proto_obj.to_segments()
        # socket.send(string_capn_proto, copy=False)
        socket.send(string_capn_proto)
        # socket.send_multipart(string_capn_proto, copy=False)
        time_send = time.process_time_ns()

        #  Get the reply
        message = socket.recv()
        # message = socket.recv_multipart(copy=False)
        # message = socket.recv(copy=False)
        cats = cat_group_capnp.CatGroup.from_bytes(message)
        capn_proto_obj = cats.as_builder()
        # cat_group_capnp.CatGroup.from_segments(message)
        end = time.process_time_ns()

        total_times.append(end - start)
        send_times.append(time_send - start)
        receive_times.append(end - time_send)

    print_data(total_times, "Proto Total Times")
    print_data(send_times, "Proto Send Times")
    print_data(receive_times, "Proto Receive Times")


if "SERVER" in os.environ:
    run_server()
else:
    run_client(100000, make_cats_request_size, cat_group_capnp.CatGroup)
    run_client(100000, make_cats_response_size, cat_group_capnp.CatGroup)
