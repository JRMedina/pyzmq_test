import random
import string

from pyzmq_test.cat_group_pb2 import Breed, Cat, CatGroup


def fill_cat(cat: Cat) -> Cat:
    cat.breed = random.choice([Breed.SIAMESE, Breed.MAINE_COON, Breed.SACRED_BIRMAN])
    cat.name = "".join(random.choices(string.ascii_uppercase + string.digits, k=20))
    return cat


def make_cats(num_cats: int) -> CatGroup:
    cat_group = CatGroup()

    while len(cat_group.cats) < num_cats:
        fill_cat(cat_group.cats.add())
    return cat_group


def make_cats_response_size() -> CatGroup:
    # 0.15 MB for 12 ads with vast
    return make_cats(6000)


def make_cats_request_size() -> CatGroup:
    # 0.0015 mb
    return make_cats(60)
