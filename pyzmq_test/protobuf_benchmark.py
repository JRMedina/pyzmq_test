import time

from typing import Callable

import numpy

from tqdm import tqdm

from pyzmq_test.benchmark_utils import get_string_size_in_mb, print_data
from pyzmq_test.cat_group_pb2 import CatGroup
from pyzmq_test.proto_func import make_cats_request_size, make_cats_response_size


ProtoObjectType = CatGroup
MakeObjectType = Callable[[], CatGroup]


def run(
    num_iterations: int,
    make_object_func: MakeObjectType,
    proto_object_class: ProtoObjectType,
) -> None:

    total_times = []
    unstruct_times = []
    struct_times = []

    proto_obj = make_object_func()
    serialized_proto = proto_obj.SerializeToString()
    print(f"Unserialized proto is {proto_obj.ByteSize() * 0.000001} MBs.")
    print(f"Serialized proto is {get_string_size_in_mb(serialized_proto)} MBs")

    for _ in tqdm(range(num_iterations)):

        start = time.process_time_ns()
        string_proto = proto_obj.SerializeToString()
        from_string = time.process_time_ns()
        proto_obj_from_string = proto_object_class()
        proto_obj_from_string.ParseFromString(string_proto)
        end = time.process_time_ns()

        total_times.append(end - start)
        unstruct_times.append(from_string - start)
        struct_times.append(end - from_string)

    print_data(total_times, "Proto Total Times")
    print_data(unstruct_times, "Proto Serial Times")
    print_data(struct_times, "Proto Deserial Times")


if __name__ == "__main__":
    run(1000, make_cats_request_size, CatGroup)
    run(1000, make_cats_response_size, CatGroup)
